function Question(text,choices,answer){
    this.text = text;
    this.choices = choices;
    this.answer = answer;
}

//Question Prototype

Question.prototype.checkAnswer = function(answer){
    return this.answer === answer;
}

//Quiz constructor
function Quiz(questions){
    this.questions = questions;
    this.score = 0;
    this.questionIndex = 0;
}

//Quiz Prototype

Quiz.prototype.getQuestion = function(){
    return this.questions[this.questionIndex];

}

//Quiz isFinish
Quiz.prototype.isFinish = function(){
    return this.questions.length ===
     this.questionIndex;
}

var q1 = new Question("what's the best programming",["C#","javascript","pyhton","asp.net"],"javascript");
var q2 = new Question("what's the most popular language ?",["C#","visual basic","nodejs","javascript"],"javascript");
var q3 = new Question("what's the best modern programming language ?",["C#","javascript","pyhton","asp.net"],"javascript");

var questions = [q1,q2,q3];

console.log(q1.checkAnswer("C#"));
console.log(q1.checkAnswer("javascript"));

console.log(q2.checkAnswer("visual basic"));
console.log(q2mm.checkAnswer("javascript"));